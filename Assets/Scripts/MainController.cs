﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace RandomDistribution
{
    public class MainController : MonoBehaviour
    {
        private const string DefaultUrl = "http://192.168.100.2:5000/upload";
        private const int DefaultDataetSize = 100000;

        [SerializeField] public InputField _url = null;
        [SerializeField] public InputField _datastSize = null;
        [SerializeField] public Text _status = null;

        public float MinValue = 0f;
        public float MaxValue = 100f;


        private IRandomGenerator[] CreateRandomGenerators()
        {
            return new IRandomGenerator[]
            {
                new SystemIntegerRandomGenerator(MinValue, MaxValue),
                new SystemDoubleRandomGenerator(MinValue, MaxValue),
                new UnityRandomRangeGenerator(MinValue, MaxValue),
                new UnityRandomValueGenerator(MinValue, MaxValue),
                new UnityMathematicsRandomValueGenerator(MinValue, MaxValue)
            };
        }

        #region Unity logic && UI

        public string Url => _url.text;

        public int DatasetSize
        {
            get
            {
                int size = int.Parse(_datastSize.text);
                return size > 0 ? size : throw new Exception("Dataset size is less than 1.");
            }
        }


        private void Awake()
        {
            _url.text = DefaultUrl;
            _datastSize.text = DefaultDataetSize.ToString();
        }

        public void OnLogTestData()
        {
            string data = GenerateCsvDataSet(10, CreateRandomGenerators());
            Debug.Log(data);
        }

        public void OnCheckConnection()
        {
            _status.text = "Checking connection...";
            //GenerateCsvData(new UnityRandomGenerator(MinValue, MaxValue));
            StartCoroutine(CheckConnectionCoroutine(Url, (result, response) => _status.text = response));
        }

        public void OnGenerateAndSubmit()
        {
            StartCoroutine(GenerateAndSubmitCoroutine(Url));
        }

        private IEnumerator GenerateAndSubmitCoroutine(string url)
        {
            bool success = false;
            string response = null;
            Action<bool, string> callback = (success1, response1) =>
            {
                success = success1;
                response = response1;
            };

            // check connection
            _status.text = "Checking connection...";
            yield return null;
            yield return CheckConnectionCoroutine(url, callback);
            if (!success)
            {
                _status.text = response;
                yield break;
            }

            _status.text = "Generating dataset...";
            yield return null;
            string data = GenerateCsvDataSet(DatasetSize, CreateRandomGenerators());

            _status.text = "Submitting dataset...";
            yield return null;
            yield return SubmitDataSetCoroutine(url, data, callback);
            if (!success)
            {
                _status.text = response;
                yield break;
            }

            _status.text = "Completed!";
        }

        #endregion

        #region Network

        private IEnumerator CheckConnectionCoroutine(string url, Action<bool, string> callback = null)
        {
            bool success = false;
            string response = null;

            using (var request = UnityWebRequest.Get(url))
            {
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    response = $"[{request.responseCode}] {request.error}";
                    success = false;
                }
                else
                {
                    response = $"[{request.responseCode}] Ok!";
                    success = true;
                }
            }

            callback?.Invoke(success, response);
        }


        private IEnumerator SubmitDataSetCoroutine(string url, string data, Action<bool, string> callback = null)
        {
            bool success = false;
            string response = null;

            var form = new List<IMultipartFormSection>
            {
                new MultipartFormDataSection("os", SystemInfo.operatingSystem),
                new MultipartFormDataSection("deviceName", SystemInfo.deviceName),
                new MultipartFormDataSection("runtimePlatform", UnityRuntimeInfo.PlatformName),
                new MultipartFormDataSection("scriptingBackend", UnityRuntimeInfo.ScriptingBackendName),
                new MultipartFormDataSection("dotNet", UnityRuntimeInfo.DotNetLevel),
                new MultipartFormFileSection("dataset", data, Encoding.UTF8, "dataset.csv")
            };

            using (var request = UnityWebRequest.Post(url, form))
            {
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    response = $"[{request.responseCode}] {request.error}";
                    success = false;
                }
                else
                {
                    response = $"[{request.responseCode}] Ok!";
                    success = true;
                }
            }

            callback?.Invoke(success, response);
        }

        #endregion

        #region DataSet generation

        private string GenerateCsvDataSet(int dataSetSize, params IRandomGenerator[] generators)
        {
            using (StringWriter writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                GenerateCsvDataSet(writer, dataSetSize, generators);
                return writer.ToString();
            }
        }


        public void GenerateCsvDataSet(string path, int dataSetSize, params IRandomGenerator[] generators)
        {
            using (var writer = File.CreateText(path))
            {
                GenerateCsvDataSet(writer, dataSetSize, generators);
            }
        }


        private void GenerateCsvDataSet(TextWriter writer, int dataSetSize, params IRandomGenerator[] generators)
        {
            const char separator = ',';
            int lastIdx = generators.Length - 1;

            // write header
            for (int j = 0; j <= lastIdx; j++)
            {
                writer.Write(generators[j].Name);
                if (j != lastIdx)
                    writer.Write(separator);
            }
            writer.WriteLine();

            // write data
            for (int i = 0; i <= dataSetSize; i++)
            {
                for (int j = 0; j <= lastIdx; j++)
                {
                    writer.Write(generators[j].Generate());
                    if (j != lastIdx)
                        writer.Write(separator);
                }

                if (i != dataSetSize)
                    writer.WriteLine();
            }
        }

        #endregion
    }
}
