﻿using UnityEngine;

namespace RandomDistribution
{
    public static class UnityRuntimeInfo
    {
        public static string PlatformName => Application.platform.ToString();

        public static string ScriptingBackendName =>
#if ENABLE_IL2CPP
            "il2cpp";
#elif ENABLE_MONO
            "mono";
#else
            "unknown";
#endif

        public static string DotNetLevel =>
#if NET_2_0_SUBSET
            "net2.0subset";
#elif NET_2_0
            "net2.0";
#elif NET_4_6
            "NET_2_0";
#elif NET_STANDARD_2_0
            "netStandard2.0";
#else
            "unknown";
#endif
    }
}
